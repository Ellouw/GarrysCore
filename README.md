[![Header](https://media.discordapp.net/attachments/832555515967963176/841269250308046848/GarrysCoreHeader.png?width=1024&height=312 "Header")](https://www.youtube.com/user/garryschool)
## GarrysCore ✨

Bienvenue sur la page GitHub officielle de "GarrysCore".

## 🚩 GarrysCore, c'est quoi ?
GarrysCore est le bot discord français et 100% unique du vidéaste et streamer "Garry's School". Ce bot a été conçu pour moderniser la modération du [Discord](https://discord.gg/Garryschool) et faciliter la tâche des modérateurs et administrateurs.

# ❤ Remerciements chaleureux à
[Kyominii](https://github.com/t-valette) pour la participation au projet ainsi qu'au développement que nous faisons ensemble main dans la main.

# ❓ Questions fréquemment posées
[Q] Le développement est-il toujours d'actualité ?

Bien sûr, le développement est constant, et en aucun cas nous n'allons interrompre le développement, si nous voyons des erreurs, nous les corrigerons en apportant des améliorations.

[Q] Les commandes, c'est quoi ?
> Pour voir **toutes les commandes** de GarrysCore, tapez simplement `%help`

> *Pour de l'aide concernant les serveurs GTAcityRP, tapez* `%gtacity`
# Qu'est-ce qui est disponible sur GarrysCore ?

- [x] Les commandes de modération 🔨
- Bannissement d'un utilisateur.
- Débannir un utilisateur.
- Expulsion d'un utilisateur.
- Mettre des avertissements.
- Retirer un/des avertissements d'un utilisateur.
- Voir les avertissements.
- [x] Système de database [Mongoose](https://www.mongodb.com/cloud/atlas/efficiency?utm_source=google&utm_campaign=gs_emea_france_search_core_brand_atlas_desktop&utm_term=mongodb&utm_medium=cpc_paid_search&utm_ad=e&utm_ad_campaign_id=12212624521&gclid=EAIaIQobChMIoNCxooW_8AIVwutRCh0CqAW1EAAYASABEgL2BfD_BwE)
- [x] Les commandes communautaire 💭
- Deux fonctionnalités `%help` & `%gtacity`
- [ ] Toutes les protections serveurs 🔒
- Toutes les fonctionnalités sont temporairement indisponibles.
- [ ] Les commandes de jeux 🎋
- [ ] Les commandes pour la musique 🎶
- Prochainement disponible.

## 🌍 Langages de programmation:
<p>
  <img alt="js" src="https://img.shields.io/badge/-Javascript-FFEE00?style=flat-square&logo=javascript&logoColor=black"/>
  <img alt="nodejs" src="https://img.shields.io/badge/-NodeJS-43853D?style=flat-square&logo=Node.js&logoColor=white"/>
</p>

## ⚙️ Outils de programmation:
<p>
  <img alt="github" width="40px" src="https://raw.githubusercontent.com/coderjojo/coderjojo/master/img/github.svg"/>
</p>

## 🎮 Les serveurs GTAcityRP

[FR] GTAcity RP - Serveur 1/2 - Serious RP | [FR] GTAcity RP - Serveur 2/2 - Serious RP
------------ | -------------
**IP:** 51.91.214.64:27015 | **IP:** 51.91.214.64:27016

## 📍 Les liens utiles de GTAcityRP
Site Internet | La Boutique | TeamSpeak
------------ | ------------- | -------------
[GTAcityRP - Forum](https://gtacityrp.fr/index.php) | [Boutique GTAcityRP](https://boutique.gtacityrp.fr/) | 149.202.139.58:10315
<p>
	<img alt="GTAcityRP" src="https://media.discordapp.net/attachments/832555515967963176/841275715777462302/GTAcityRP.png?width=1024&height=444"/>
</p>
