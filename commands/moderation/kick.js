const Discord = require('discord.js');
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    const mentionedMember = message.mentions.members.first()
        || message.guild.members.cache.get(args[0])

    if (!message.member.hasPermission('KICK_MEMBERS')) {
        return message.channel.send(new Discord.Message()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous n\'avez pas la permission d\'expulser des utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!message.guild.me.hasPermission('KICK_MEMBERS')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je n\'ai pas la permission de virer des utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!mentionedMember) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous devez mentionner un utilisateur que vous voulez expulser.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const mentionedPosition = mentionedMember.roles.highest.position
    const memberPotition = message.member.roles.highest.position
    const botPotision = message.guild.me.roles.highest.position

    if (memberPotition <= mentionedPosition) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous ne pouvez pas virer l\'utilisateur parce que son rôle est supérieur au vôtre.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (botPotision <= mentionedPosition) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Tu ne peux pas virer l\'utilisateur parce que son rôle est supérieur au mien.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const reason = args.slice(1).join(' ') || 'Aucune spécification'

    try {
        await mentionedMember.kick([reason])

        message.channel.send(new Discord.MessageEmbed()
            .setAuthor(`${message.author.tag}`, message.author.avatarURL())
            .addField("**Utilisateur**", `${mentionedMember}`)
            .addField("**Action**", "Expulsion")
            .addField("**Raison**", `${reason ? `${reason}` : ''}`)
            .setThumbnail(mentionedMember.user.displayAvatarURL({ dynamic: true }))
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.kick))
            
    }
    catch (error) {
        console.log(error)
        message.channel.send('Il y a eu une erreur dans l\'expulsion de l\'utilisateur !')
    }

}

module.exports.help = {
    name: "kick"
}