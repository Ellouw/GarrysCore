const Discord = require('discord.js');
const ms = require('ms');
const muteModel = require('../../models/mute');
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    const mentionedMember = message.mentions.members.first()
        || message.guild.members.cache.get(args[0])

    const msRegex = RegExp(/(\d+(s|m|h|w))/)
    let muteRole = message.guild.roles.cache.find(r => r.name == 'Mute')

    if (!message.member.hasPermission('MANAGE_ROLES')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous n\'avez pas la permission de mettre en sourdine des utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!message.guild.me.hasPermission(['MANAGE_ROLES', 'MANAGE_CHANNELS'])) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je n\'ai pas les permissions de `MANAGE_ROLES` et `MANAGE_CHANNELS`.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!mentionedMember) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous devez mentionner un utilisateur que vous voulez mettre en sourdine.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!msRegex.test(args[1])) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Ce n\'est pas une durée valable pour mettre un utilisateur en sourdine.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    if (!muteRole) {
        muteRole = await message.guild.roles.create({
            data: {
                name: 'Mute',
                color: 'BLACK',
            }
        }).catch(err => console.log(err))
    }

    if (mentionedMember.roles.highest.position >= message.guild.me.roles.highest.position) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je ne peux pas mettre l\'utilisateur en sourdine car son rôle est supérieur au mien.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (muteRole.position >= message.guild.me.roles.highest.position) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je ne peux pas rendre l\'utilisateur en sourdine parce que le rôle "Muet" est supérieur au mien.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (ms(msRegex.exec(args[1])[1]) > 2592000000) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous ne pouvez pas mettre l\'utilisateur en sourdine pendant plus d\'un mois.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const isMuted = await muteModel.findOne({
        guildID: message.guild.id,
        memberID: mentionedMember.id,
    })

    if (isMuted) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Cet utilisateur est déjà en sourdine.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    for (const channel of message.guild.channels.cache) {
        channel[1].updateOverwrite(muteRole, {
            SEND_MESSAGES: false,
            CONNECT: false,
        }).catch(err => console.log(err))
    }

    const noEveryone = mentionedMember.roles.cache.filter(r => r.name !== '@everyone')

    await mentionedMember.roles.add(muteRole.id).catch(err => console.log(err))

    for (const role of noEveryone) {
        await mentionedMember.roles.remove(role[0]).catch(err => console.log(err))
    }

    const muteDoc = new muteModel({
        guildID: message.guild.id,
        memberID: mentionedMember.id,
        length: Date.now() + ms(msRegex.exec(args[1])[1]),
        memberRoles: noEveryone.map(r => r),
    })

    await muteDoc.save().catch(err => console.log(err))

    const reason = args.slice(2).join(' ') || 'Aucune spécification'

    message.channel.send(new Discord.MessageEmbed()
        .setAuthor(`${message.author.tag}`, message.author.avatarURL())
        .addField("**Utilisateur**", `${mentionedMember}`)
        .addField("**Action**", "Mute")
        .addField("**Raison**", `${reason ? `${reason}` : ''}`)
        .addField("**Temps**", `${msRegex.exec(args[1])[1]}`)
        .setThumbnail(mentionedMember.user.displayAvatarURL({ dynamic: true }))
        .setTimestamp(message.createdAt)
        .setFooter()
        .setColor(colors.mute))

}

module.exports.help = {
    name: "mute"
}