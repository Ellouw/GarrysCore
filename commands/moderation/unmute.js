const muteModel = require('../../models/mute');
const Discord = require('discord.js');
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    const mentionedMember = message.mentions.members.first()
        || message.guild.members.cache.get(args[0])

    const muteRole = message.guild.roles.cache.find(r => r.name == 'Mute')

    if (!message.member.hasPermission('MANAGE_ROLES')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous n\'avez pas la permission de rendre les utilisateurs unmute.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!mentionedMember) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous devez mentionner l\'utilisateur que vous voulez rendre unmute.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!muteRole) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Ce serveur n\'a pas de rôle `Mute`, une fois que vous avez mis un utilisateur en sourdine, un rôle sera créé !')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const muteDoc = await muteModel.findOne({
        guildID: message.guild.id,
        memberID: mentionedMember.id,
    })

    if (!muteDoc) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Cet utilisateur n\'est pas en sourdine.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (mentionedMember.roles.highest.potision >= message.guild.me.roles.highest.potision) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je ne peux pas rétablir la parole de cet utilisateur parce que son rôle est supérieur au mien.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (muteRole.potision >= message.guild.me.roles.highest.potision) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je ne peux pas rétablir la parole des utilisateurs parce que le rôle `Mute` est supérieur au mien.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    mentionedMember.roles.remove(muteRole.id).catch(err => console.log(err))

    for (const role of muteDoc.memberRoles) {
        mentionedMember.roles.add(role).catch(err => console.log(err))
    }

    await muteDoc.deleteOne()

    const reason = args.slice(1).join(' ') || 'Aucune spécification'

    message.channel.send(new Discord.MessageEmbed()
        .setAuthor(`${message.author.tag}`, message.author.avatarURL())
        .addField("**Utilisateur**", `${mentionedMember}`)
        .addField("**Action**", "Unmute")
        .addField("**Raison**", `${reason ? `${reason}` : ''}`)
        .setTimestamp(message.createdAt)
        .setThumbnail(mentionedMember.user.displayAvatarURL({ dynamic: true }))
        .setFooter()
        .setColor(colors.unmute))

}

module.exports.help = {
    name: "unmute"
}