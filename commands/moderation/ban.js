const userReg = RegExp(/<@!?(\d+)>/);
const Discord = require('discord.js');
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    const userID = userReg.test(args[0]) ? userReg.exec(args[0])[1] : args[0]
    const mentionedUser = await message.client.users.fetch(userID)

    if (!message.member.hasPermission('BAN_MEMBERS')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous n\'avez pas la permission de bannir des utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!message.guild.me.hasPermission('BAN_MEMBERS')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je n\'ai pas la permission de bannir des utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!mentionedUser) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous devez mentionner un utilisateur à bannir.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const allBans = await message.guild.fetchBans()

    if (allBans.get(mentionedUser.id)) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** L\'utilisateur est déjà banni.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const mentionedMember = message.guild.members.cache.get(mentionedUser.id)

    if (mentionedMember) {
        const mentionedPotision = mentionedMember.roles.highest.position
        const memberPosition = message.member.roles.highest.position
        const botPotision = message.guild.me.roles.highest.position

        if (memberPosition <= mentionedPotision) {
            return message.channel.send(new Discord.MessageEmbed()
                .setAuthor('Error 403', message.author.avatarURL())
                .setDescription('**➥** Vous ne pouvez pas bannir l\'utilisateur parce que son rôle est supérieur au vôtre.')
                .setTimestamp(message.createdAt)
                .setFooter()
                .setColor(colors.red))
        }
        else if (botPotision <= mentionedPotision) {
            return message.channel.send(new Discord.MessageEmbed()
                .setAuthor('Error 403', message.author.avatarURL())
                .setDescription('**➥** Je ne peux pas bannir l\'utilisateur parce que son rôle est supérieur au mien.')
                .setTimestamp(message.createdAt)
                .setFooter()
                .setColor(colors.red))
        }
    }

    const reason = args.slice(1).join(' ') || 'Aucune spécification'

    message.guild.members.ban(mentionedUser.id, { reason: reason })

    message.channel.send(new Discord.MessageEmbed()
        .setAuthor(`${message.author.tag}`, message.author.avatarURL())
        .addField('**Utilisateur**', `${mentionedUser}`)
        .addField("**Action**", "Ban")
        .addField("**Raison**", `${reason ? `${reason}` : ''}`)
        .setTimestamp(message.createdAt)
        .setThumbnail(mentionedUser.displayAvatarURL({ dynamic: true }))
        .setFooter()
        .setColor(colors.ban))
        
}

module.exports.help = {
    name: "ban"
}