const userReg = RegExp(/<@!?(\d+)>/);
const Discord = require('discord.js');
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    const userID = userReg.test(args[0]) ? userReg.exec(args[0])[1] : args[0]
    const mentionedUser = await message.client.users.fetch(userID).catch(() => null)

    if (!message.member.hasPermission('BAN_MEMBERS')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥**  Vous n\'avez pas la permission de débannir les utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!message.guild.me.hasPermission('BAN_MEMBERS')) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Je n\'ai pas la permission de débannir les utilisateurs.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }
    else if (!mentionedUser) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** Vous devez mentionner un utilisateur à débannir.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const allBans = await message.guild.fetchBans()
    const bannedUser = allBans.get(mentionedUser.id)

    if (!bannedUser) {
        return message.channel.send(new Discord.MessageEmbed()
            .setAuthor('Error 403', message.author.avatarURL())
            .setDescription('**➥** L\'utilisateur n\'est pas banni.')
            .setTimestamp(message.createdAt)
            .setFooter()
            .setColor(colors.red))
    }

    const reason = args.slice(1).join(' ') || 'Aucune spécification'

    message.guild.members.unban(mentionedUser.id, [reason]).catch(err => console.log(err))

    message.channel.send(new Discord.MessageEmbed()
        .setAuthor(`${message.author.tag}`, message.author.avatarURL())
        .addField("**Utilisateur**", `${mentionedUser}`)
        .addField("**Action**", "Unban")
        .addField("**Raison**", `${reason ? `${reason}` : ''}`)
        .setThumbnail(mentionedUser.displayAvatarURL({ dynamic: true }))
        .setTimestamp(message.createdAt)
        .setFooter()
        .setColor(colors.unban))

}

module.exports.help = {
    name: "unban"
}