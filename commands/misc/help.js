const Discord = require("discord.js");
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    let helpMessage = new Discord.MessageEmbed()
        .setTitle("GarrysCore")
        .setURL()
        .setColor(colors.blue)
        .addFields(
            { name: "🎈 Communauté",
            value: `> **\`%gtacity\`** - Affiche les informations de GTAcityRP.`},
        )
        .addFields(
            { name: "🔨 Modération",
            value: `> **\`%ban [@user] [raison]\`** - Bannir un utilisateur de type **permanent**.
            > **\`%unban [@user] [raison]\`** - Débannir un utilisateur du serveur.
            > **\`%kick [@user] [reason]\`** - Expulsion d'un utilisateur.
            > **\`%mute [@user] [length] [reason]\`** - Mettre en sourdine un utilisateur pour qu'il cesse d'écrire dans les salons.
            > **\`%unmute [@user] [reason]\`** - Enlever la sourdine pour réécrire sur les salons.
            > **\`%warn [@user] [reason]\`** - Mettre un avertissement à un utilisateur.
            > **\`%unwarn [@user] [id] [reason]\`** - Enlever l'avertissement d'un utilisateur.
            > **\`%warnings [@user]\`** - Voir les avertissements de l'utilisateur spécialisé.`},
        )
        .setFooter("By @Ellouw on GitHub | Dernière mise à jour le: 9 mai 2021")
        .setTimestamp(message.createdAt)

    return message.channel.send(helpMessage);
    
}

module.exports.help = {
    name: "help"
}