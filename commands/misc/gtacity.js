const Discord = require("discord.js");
const colors = require('../../settings/colors.json');

module.exports.run = async (client, message, args) => {

    let gtacityMessage = new Discord.MessageEmbed()
        .setTitle("Toutes les informations de GTAcityRP")
        .setAuthor(message.member.displayName, message.author.displayAvatarURL())
        .setThumbnail(`https://cdn.discordapp.com/attachments/840912514745106432/840912541197664256/GTAcityRP.png`)
        .setColor(colors.blue)
        .addFields(
            { name: '[FR] GTAcity RP - Serveur 1/2 - Serious RP', value: '51.91.214.64:27015', inline: true },
            { name: '[FR] GTAcity RP - Serveur 2/2 - Serious RP', value: '51.91.214.64:27016', inline: true },
        )
        .addField("📍 Retrouvez l'ensemble des liens de GTAcityRP",
        `[Télécharge les addons](https://steamcommunity.com/sharedfiles/filedetails/?id=1450112408)
            [Notre site](https://gtacityrp.fr/index.php)
                [Boutique GTAcityRP](https://boutique.gtacityrp.fr/)`)
        .addField("🔖 Un problème ? Une question ?", "**Notre TeamSpeak:** 149.202.139.58:10315")
        .addField('RAPPEL',
        `Le discord __Garryschool__ n'est pas celui de GTAcityRP.
        **GTAcityRP** ne possède aucun serveur __discord__ mais seulement d'un TeamSpeak.`)
        .setImage(`https://media.discordapp.net/attachments/840912514745106432/841042501754421248/LoadingServer.png?width=682&height=383`)
        .setFooter("© 2014-2021 GTACITY RP INC. TOUS DROITS RÉSERVÉS.", `https://cdn.discordapp.com/attachments/840912514745106432/840930537526132756/GMOD.png`)
        .setTimestamp(message.createdAt)

    return message.channel.send(gtacityMessage);
    
}

module.exports.help = {
    name: "gtacity"
}